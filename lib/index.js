"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.pipe = void 0;
var ramda_1 = require("ramda");
Object.defineProperty(exports, "pipe", { enumerable: true, get: function () { return ramda_1.pipe; } });
